/* eslint-disable @next/next/no-img-element */
import Layout from "../components/Layout";
import Link from "next/dist/client/link";

export default function Home({ res }) {
  console.log(res);
  return (
    <>
      <Layout page="Crypto-wb - Accueil">
        <ul className="grid grid-cols-4 gap-10 justify-around py-10 relative top-10 mygrid">
          {res.map((crypto, index) => (
            <li
              key={index}
              className="relative hover:shadow-md p-8 border-2 border-yellow-300 rounded-3xl bg-grey-100 md:w-auto flex-1 mx-5"
            >
              <Link href={`/${crypto.name}?id=${crypto.id}`}>
                <a className="rounded-md">
                  <div className="text-center">
                    <img
                      src={crypto.logo_url}
                      alt={crypto.name}
                      className="w-20 h-20 mx-auto mb-6"
                    />
                    <h2 className="text-2xl mb-6 uppercase tracking-wider">
                      {crypto.name}
                    </h2>
                    <h3 className="font-bold text-2xl mb-4">
                      {parseFloat(crypto.price).toFixed(2)} USD
                    </h3>
                    <p>
                      1 jour :{" "}
                      <span className="font-bold">
                        {parseFloat(
                          crypto["1d"].price_change_pct * 100
                        ).toFixed(2) + "%"}
                      </span>
                      {crypto["1d"].price_change_pct < 0 ? (
                        <span className="text-red-500"> &#x2798;</span>
                      ) : (
                        <span className="text-green-500"> &#x297A;</span>
                      )}
                    </p>
                    <p>
                      1 mois :{" "}
                      <span className="font-bold">
                        {parseFloat(
                          crypto["30d"].price_change_pct * 100
                        ).toFixed(2) + "%"}
                      </span>
                      {crypto["1d"].price_change_pct < 0 ? (
                        <span className="text-red-500"> &#x2798;</span>
                      ) : (
                        <span className="text-green-500"> &#x297A;</span>
                      )}
                    </p>
                    <p>
                      1 an :{" "}
                      <span className="font-bold">
                        {parseFloat(
                          crypto["365d"].price_change_pct * 100
                        ).toFixed(2) + "%"}
                      </span>
                      {crypto["1d"].price_change_pct < 0 ? (
                        <span className="text-red-500"> &#x2798;</span>
                      ) : (
                        <span className="text-green-500"> &#x297A;</span>
                      )}
                    </p>
                  </div>
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </Layout>
    </>
  );
}

//server side rendered
export async function getStaticProps(context) {
  try {
    const res = await fetch(
      `https://api.nomics.com/v1/currencies/ticker?key=${process.env.API_KEY_NOMICS}&ids=BTC,ETH,AAVE,DOGE,XRP,DOT,BNB,USDC,ADA,USDT&interval=1d,30d,365d`
    ).then((res) => res.json());

    return {
      props: { res },
    };
  } catch (err) {
    console.error(err);
  }
}
