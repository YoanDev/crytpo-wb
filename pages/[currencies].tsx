/* eslint-disable @next/next/no-img-element */
import React from "react";
import Layout from "../components/Layout";

interface Props {
  res: any;
}

const Currencies = ({ res }: Props) => {
  return (
    <Layout page={`Page : ${res.name}`}>
      <div className="p-8">
        <div className="relative hover:shadow md p-8 border border-yellow-300 sm:rounded-3xl bg-black text-white md:w-auto flex-1 mx-5 top-10">
          <div className="text-center">
            <img
              src={res.logo_url}
              alt={res.name}
              className="w-20 h-20 mx-auto mb-6"
            />
          </div>
          <h2 className="text-2xl mb-6 uppercase tracking-wider">{res.name}</h2>
          <p>{res.description}</p>
          <p className="pt-5 text-blue-500">
            <a href={res.reddit_url} target="_blank" rel="noreferrer">
              {res.reddit_url}
            </a>
          </p>
        </div>
      </div>
    </Layout>
  );
};

export default Currencies;

interface QueryProps {
  query: any
}
export async function getServerSideProps({ query }: QueryProps) {
  try {
    const res = await fetch(
      `https://api.nomics.com/v1/currencies?key=${process.env.API_KEY_NOMICS}&ids=${query.id}&attributes=id,name,logo_url,description,reddit_url`
    );
    const result = await res.json();
    return {
      props: { res: result[0] },
    };
  } catch (err) {
    console.error(err);
  }
}
