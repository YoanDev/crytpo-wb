import React, { useEffect } from "react";
import Layout from "../components/Layout";

const About = () => {
  return (
    <>
      <Layout page="Crypto-wb - About">
        <div className="flex flex-col p-10 relative top-10 items-center content-center min-h-screen">
          <h2 className="relative text-5xl mb-10">Culture</h2>
          <h3 className="text-lg mb-32">
            Discover the world of cryptocurrencies and develop your financial
            culture
          </h3>
          <div className="grid grid-cols-2 h-full gap-32">
            <div>
              <a
                href="https://en.wikipedia.org/wiki/Cryptocurrency"
                target="_blank"
                rel="noreferrer"
              >
                <h3 className="text-yellow-300 mb-10 text-3xl cursor-pointer hover:text-yellow-400">
                  Cryptocurrency
                </h3>
              </a>
              <p>
                A cryptocurrency, crypto-currency, or crypto is a digital asset
                designed to work as a medium of exchange wherein individual coin
                ownership records are stored in a ledger existing in a form of a
                computerized database using strong cryptography to secure
                transaction records, to control the creation of additional
                coins, and to verify the transfer of coin ownership.
                Cryptocurrency does not exist in physical form (like paper
                money) and is typically not issued by a central authority.
                Cryptocurrencies typically use decentralized control as opposed
                to a central bank digital currency (CBDC). When a cryptocurrency
                is minted or created prior to issuance or issued by a single
                issuer, it is generally considered centralized. When implemented
                with decentralized control, each cryptocurrency works through
                distributed ledger technology, typically a blockchain, that
                serves as a public financial transaction database.
              </p>
            </div>
            <div>
              <a
                href="https://en.wikipedia.org/wiki/Financial_asset"
                target="_blank"
                rel="noreferrer"
              >
                <h3 className="text-yellow-300 mb-10 text-3xl cursor-pointer hover:text-yellow-400">
                  Financial asset
                </h3>
              </a>
              <p>
                A cryptocurrency, crypto-currency, or crypto is a digital asset
                designed to work as a medium of exchange wherein individual coin
                ownership records are stored in a ledger existing in a form of a
                computerized database using strong cryptography to secure
                transaction records, to control the creation of additional
                coins, and to verify the transfer of coin ownership.
                Cryptocurrency does not exist in physical form (like paper
                money) and is typically not issued by a central authority.
                Cryptocurrencies typically use decentralized control as opposed
                to a central bank digital currency (CBDC). When a
                cryptocurrency is minted or created prior to issuance or issued
                by a single issuer, it is generally considered centralized. When
                implemented with decentralized control, each cryptocurrency
                works through distributed ledger technology, typically a
                blockchain, that serves as a public financial transaction
                database.
              </p>
            </div>
            <div>
              <a
                href="https://en.wikipedia.org/wiki/Cryptocurrency"
                target="_blank"
                rel="noreferrer"
              >
                <h3 className="text-yellow-300 mb-10 text-3xl cursor-pointer hover:text-yellow-400">
                  {"Currency"}
                </h3>
              </a>
              <p>
                A currencyin the most specific sense is money in any form when
                in use or circulation as a medium of exchange, especially
                circulating banknotes and coins. A more general definition is
                that a currency is a system of money (monetary units) in common
                use, especially for people in a nation. Under this definition,
                U.S. dollars (US$), euros (€), Indian Rupee (₹), Japanese yen
                (¥), and pounds sterling (£) are examples of currencies.
                Currencies may act as stores of value and be traded between
                nations in foreign exchange markets, which determine the
                relative values of the different currencies. Currencies in this
                sense are defined by governments, and each type has limited
                boundaries of acceptance.
              </p>
            </div>
            <div>
              <a
                href="https://en.wikipedia.org/wiki/Stock_market"
                target="_blank"
                rel="noreferrer"
              >
                <h3 className="text-yellow-300 mb-10 text-3xl cursor-pointer hover:text-yellow-400">
                  {"Stock market"}
                </h3>
              </a>
              <p>
                A stock market, equity market, or share market is the
                aggregation of buyers and sellers of stocks (also called
                shares), which represent ownership claims on businesses; these
                may include securities listed on a public stock exchange, as
                well as stock that is only traded privately, such as shares of
                private companies which are sold to investors through equity
                crowdfunding platforms. Investment in the stock market is most
                often done via stockbrokerages and electronic trading platforms.
                Investment is usually made with an investment strategy in mind.
              </p>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default About;
