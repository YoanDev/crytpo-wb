import React from "react";
import Head from "next/head";
import { Navbar } from "./Navbar";
import Footer from "./Footer";
import Hero from "./Hero";

interface Props {
  page: string;
  children?: any;
}
const Layout = ({ page, children }: Props) => {
  return (
    <>
      <Head>
        <title>{page}</title>
      </Head>
      <Navbar />
      <Hero/>
      <div className="relative w-full min-h-screen p-10">{children}</div>
      <Footer></Footer>
    </>
  );
};

export default Layout;
