import React from 'react'
import Image from 'next/image'

const Hero = () => {
    return (
        <div className="top-20 relative bg-black h-30 w-full flex items-center justify-center shadow-2xl">
            <p>See the news about crypto currencies and </p>
            <Image src={"/hero.jpg"} alt="header" width="600" height="300" className="rounded-3xl object-cover" quality={100} />
        </div>
    )
}

export default Hero
