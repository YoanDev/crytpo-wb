import React from 'react'

const Footer = () => {
    return (
        <>
            <div className="relative p-10 items-center flex-col content-center flex bg-black text-yellow-300">
                <h3 className="text-sm my-0.3 cursor-pointer hover:underline">All right deserved - 2021</h3>
                <h4 className="text-sm my-0.3 cursor-pointer hover:underline">Legal Notice</h4>
                <div className="text-sm my-0.3">YMS</div>
            </div>
        </>
    )
}

export default Footer
