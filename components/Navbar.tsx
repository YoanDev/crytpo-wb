/* eslint-disable @next/next/link-passhref */
import React from 'react'
import Link from 'next/link'

export const Navbar = () => {
    return (
        <div className="fixed flex h-20 items-center content-center p-10 bg-black w-full text-yellow-300 z-50">
            <Link href="/">
                <a className="text-sm uppercase relative px-8">Home</a>
            </Link>
            <Link href="/culture">
                <a className="text-sm uppercase relative px-8">Culture</a>
            </Link>
        </div>
    )
}
